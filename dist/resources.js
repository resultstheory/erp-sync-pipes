'use strict';

module.exports = {
  local: 'Resources',
  remote: 'Employee',
  fields: ['EmpID', 'Company', 'FirstName', 'MiddleInitial', 'LastName', 'Phone', 'ShortChar01', 'EmailAddress', 'Address', 'Address2', 'City', 'State', 'VendorNum_c', 'PortalUsername_c', 'PortalPassword_c', 'PortalTerms_c', 'Status', 'SupervisorID', 'RestrictToOwner_c', 'PassportNumber_c', 'PassportExpirationDate_c', 'DriversLicenceState_c', 'DriversLicenceNumber_c', 'EgnyteFolder_c', 'Shift', 'ExpenseCode', 'CreateDate_c', 'CreatedBy_c', 'ModifyDate_c', 'ModifiedBy',
  // array of related roles
  // ideally we should select a subset of fields from that one
  'EmpRole'],
  cleanse: function cleanse(resource) {
    return resource;
  },
  map: {
    VendURL: 'Website',
    Phone: 'PhoneNum'
  },
  joints: [
    // {
    //   lookupField: 'TermsCode', parentEntity: 'Terms', parentFieldName: 'Terms', parentFields: ['TermsCode', 'Description']
    // },
    // {
    //   lookupField: 'CurrencyCode', parentEntity: 'Currencies', parentFieldName: 'Currency', parentFields: ['CurrencyCode', 'CurrDesc']
    // },
    // {
    //   lookupField: 'TerritoryID', parentEntity: 'Territories', parentFieldName: 'Territory', parentFields: ['TerritoryID', 'TerritoryDesc']
    // },
  ]
};