'use strict';

module.exports = function splitAddress(address) {
  if (!address) return [];
  var result = address.split(/\n/);
  if (result.length > 3) {
    return [result[0], result[1], result.slice(2).join('\n')];
  }
  return result;
};