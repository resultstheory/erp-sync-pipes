'use strict';

// had coded customer type map from Epicor
module.exports = function mapCustType(source) {
  var reverse = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  var def = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : source;

  var map = void 0;
  if (reverse) {
    map = {
      'Suspect': 'SUS',
      'Customer': 'CUS',
      'Prospect': 'PRO'
    };
  } else {
    map = {
      'SUS': 'Suspect',
      'CUS': 'Customer',
      'PRO': 'Prospect'
    };
  }
  return map[source] || def;
};