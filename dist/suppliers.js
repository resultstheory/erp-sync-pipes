'use strict';

var joinAddress = require('./joinAddress.js'),
    splitAddress = require('./splitAddress.js'),
    mapCustType = require('./mapCustType.js');

module.exports = {
  local: 'Suppliers',
  remote: 'Supplier',
  fields: ['VendorNum', 'Company', 'VendorID', 'Name', 'PhoneNum', 'CustURL', 'Address1', 'Address2', 'Address3', 'City', 'State', 'Zip', 'TerritoryID', 'TermsCode', 'CurrencyCode', 'SysRevID'],
  cleanse: function cleanse(supplier) {
    // clean address: combine the Address1, 2 and 3 and remove the original fields (?)
    supplier.Street = joinAddress(supplier.Address1, supplier.Address2, supplier.Address3);
    for (var k = 1; k <= 3; k++) {
      delete supplier['Address' + k];
    }
    return supplier;
  },
  prepare: function prepare(supplier, action) {
    // split address field into the address1, 2 and 3
    // populate the termscode, currencycode, vCustType and TerritoryID fields, based on the object values
    if (!supplier.VendorID && action === 'insert') supplier.VendorID = '0';
    if (supplier.Street !== undefined) ;
    var _splitAddress = splitAddress(supplier.Street);

    supplier.Address1 = _splitAddress[0];
    supplier.Address2 = _splitAddress[1];
    supplier.Address3 = _splitAddress[2];

    return supplier;
  },
  map: {
    VendURL: 'Website',
    Phone: 'PhoneNum'
  },
  joints: [
    // {
    //   lookupField: 'TermsCode', parentEntity: 'Terms', parentFieldName: 'Terms', parentFields: ['TermsCode', 'Description']
    // },
    // {
    //   lookupField: 'CurrencyCode', parentEntity: 'Currencies', parentFieldName: 'Currency', parentFields: ['CurrencyCode', 'CurrDesc']
    // },
    // {
    //   lookupField: 'TerritoryID', parentEntity: 'Territories', parentFieldName: 'Territory', parentFields: ['TerritoryID', 'TerritoryDesc']
    // },
  ]
};