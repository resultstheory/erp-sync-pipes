'use strict';

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var customers = require('./customers'),
    suppliers = require('./suppliers'),
    resources = require('./resources'),
    roles = require('./roles'),
    lookupTables = require('./lookupTables'),
    addIdMapping = require('./utils/addIdMapping');
module.exports = [].concat(_toConsumableArray(lookupTables), [customers, suppliers, resources, roles]).map(addIdMapping);