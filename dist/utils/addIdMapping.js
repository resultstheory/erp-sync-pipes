'use strict';

// ensure we have a map for the Id field
// this helps ensure that every local collection can be referred by the Id field,
// which makes the task of building lookups in the portal a lot simpler
module.exports = function addIdMapping(coll) {
  if (!coll.map) coll.map = {};
  for (var k in coll.map) {
    if (coll.map[k] === 'Id') return coll;
  }
  coll.map[coll.fields[0]] = 'Id';
  return coll;
};