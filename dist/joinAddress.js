'use strict';

module.exports = function joinAddress() {
  for (var _len = arguments.length, parts = Array(_len), _key = 0; _key < _len; _key++) {
    parts[_key] = arguments[_key];
  }

  return parts.filter(function (x) {
    return x;
  }).join('\n');
};