'use strict';

var joinAddress = require('./joinAddress.js'),
    splitAddress = require('./splitAddress.js');

module.exports = {
  local: 'Customers',
  remote: 'Customer',
  fields: ['CustNum', 'Company', 'CustID', 'Name', 'PhoneNum', 'CustURL', 'Address1', 'Address2', 'Address3', 'City', 'State', 'Zip', 'BTAddress1', 'BTAddress2', 'BTAddress3', 'BTCity', 'BTState', 'BTZip', 'TerritoryID', 'vCustType', 'TermsCode', 'CurrencyCode', 'SysRevID', 'ShipViaCode'],
  cleanse: function cleanse(customer) {
    // clean address: combine the Address1, 2 and 3 and remove the original fields (?)
    customer.Street = joinAddress(customer.Address1, customer.Address2, customer.Address3);
    customer.BTStreet = joinAddress(customer.BTAddress1, customer.BTAddress2, customer.BTAddress3);
    for (var k = 1; k <= 3; k++) {
      delete customer['Address' + k];
      delete customer['BTAddress' + k];
    }
    customer.HasBTAddress = !!(customer.BTStreet || customer.BTCity || customer.BTState);
    return customer;
  },
  prepare: function prepare(customer, action) {
    // split address field into the address1, 2 and 3
    // populate the termscode, currencycode, vCustType and TerritoryID fields, based on the object values
    if (!customer.CustID && action === 'insert') customer.CustID = '0';
    if (customer.Street !== undefined) ;
    var _splitAddress = splitAddress(customer.Street);

    customer.Address1 = _splitAddress[0];
    customer.Address2 = _splitAddress[1];
    customer.Address3 = _splitAddress[2];

    if (customer.HasBTAddress !== undefined) {
      if (customer.HasBTAddress) {
        if (customer.BTStreet !== undefined) ;
        var _splitAddress2 = splitAddress(customer.BTStreet);

        customer.BTAddress1 = _splitAddress2[0];
        customer.BTAddress2 = _splitAddress2[1];
        customer.BTAddress3 = _splitAddress2[2];

        if (!customer.BTName) customer.BTName = customer.Name;
      } else {
        customer.BTAddress1 = null;
        customer.BTAddress2 = null;
        customer.BTAddress3 = null;
        customer.BTCity = null;
        customer.BTState = null;
        customer.BTZip = null;
        customer.BTName = null;
      }
    }
    return customer;
  },
  map: {
    CustNum: 'Id',
    CustURL: 'Website',
    Phone: 'PhoneNum',
    vCustType: 'Status'
  },
  joints: [
    // {
    //   lookupField: 'TermsCode', parentEntity: 'Terms', parentFieldName: 'Terms', parentFields: ['TermsCode', 'Description']
    // },
    // {
    //   lookupField: 'CurrencyCode', parentEntity: 'Currencies', parentFieldName: 'Currency', parentFields: ['CurrencyCode', 'CurrDesc']
    // },
    // {
    //   lookupField: 'TerritoryID', parentEntity: 'Territories', parentFieldName: 'Territory', parentFields: ['TerritoryID', 'TerritoryDesc']
    // },
    // {
    //   lookupField: 'ShipViaCode', parentEntity: 'ShipVia', parentFieldName: 'ShipVia', parentFields: ['ShipViaCode', 'Description']
    // }
  ]
};