const customer = require('../src/customers')

describe('customer mapping', () => {
  it('joins address on cleanse', () => {
    const source = {
      Address1 : '123 Main Street',
      Address2 : 'Suite 200',
      Address3 : 'Building A',
      BTAddress1 : '129 Pine Street'
    }
    const result = customer.cleanse(source)
    expect(result.Street).to.equal('123 Main Street\nSuite 200\nBuilding A')
    expect(result.BTStreet).to.equal('129 Pine Street')
    expect(result).to.not.have.property('Address1')
    expect(result).to.not.have.property('Address2')
    expect(result).to.not.have.property('Address3')
    expect(result).to.not.have.property('BTAddress1')
    expect(result.HasBTAddress).to.equal(true)
  })

  it('splits address on prepare', () => {
    const source = {
      Street : '123 Main Street\nSuite 200\nBuilding A',
      BTStreet : '123 Main Street\nSuite 200\nBuilding A\nAttn: Billing',
      HasBTAddress: true
    }
    const result = customer.prepare(source)
    expect(result.Address1, 'Primary Address').to.equal('123 Main Street')
    expect(result.Address3).to.equal('Building A')
    expect(result.BTAddress1, 'Billing Address').to.equal('123 Main Street')
    expect(result.BTAddress3).to.equal('Building A\nAttn: Billing')
  })
})
