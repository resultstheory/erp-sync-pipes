const pipes = require('../src/index')

describe('pipes', () => {
  it('has customer pipe', () => {
    const customer = pipes.find(p => p.local === 'Customers')
    expect(customer).to.be.ok.and.to.be.an('object')
  })

  it('has terms pipe', () => {
    const term = pipes.find(p => p.local === 'Terms')
    expect(term).to.be.ok.and.to.be.an('object')
  })
})
