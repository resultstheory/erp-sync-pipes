module.exports = [
  {
    local: 'Territories',
    remote: 'SalesTerritory',
    fields: [
      'TerritoryID', 'Company', 'TerritoryDesc', 'Inactive', 'SysRevID'
    ]
  },
  {
    local: 'Terms',
    remote: 'Terms',
    fields: [
      'TermsCode', 'Company', 'Description', 'SysRevID'
    ]
  },
  {
    local: 'Currencies',
    remote: 'Currency',
    fields: [
      'CurrencyCode', 'CurrDesc', 'CurrSymbol'
    ]
  },
  {
    local: 'ShipVia',
    remote: 'ShipVia',
    fields: [
      'ShipViaCode', 'Company', 'Description', 'SysRevID'
    ]
  }
]
