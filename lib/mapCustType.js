// had coded customer type map from Epicor
module.exports = function mapCustType(source, reverse = false, def = source) {
  let map
  if(reverse) {
    map = {
      'Suspect': 'SUS',
      'Customer': 'CUS',
      'Prospect': 'PRO'
    }
  } else {
    map = {
      'SUS': 'Suspect',
      'CUS': 'Customer',
      'PRO': 'Prospect'
    }
  }
  return map[source] || def
}
