module.exports = function joinAddress(...parts) {
  return parts.filter(x => x).join('\n')
}
