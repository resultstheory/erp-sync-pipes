const joinAddress = require('./joinAddress.js'),
  splitAddress = require('./splitAddress.js')

module.exports = {
  local: 'Customers',
  remote: 'Customer',
  fields: [
    'CustNum', 'Company', 'CustID', 'Name', 'PhoneNum', 'CustURL',
    'Address1', 'Address2', 'Address3', 'City', 'State', 'Zip',
    'BTAddress1', 'BTAddress2', 'BTAddress3', 'BTCity', 'BTState', 'BTZip',
    'TerritoryID', 'vCustType', 'TermsCode', 'CurrencyCode', 'SysRevID',
    'ShipViaCode'
  ],
  cleanse: function(customer) {
    // clean address: combine the Address1, 2 and 3 and remove the original fields (?)
    customer.Street = joinAddress(customer.Address1, customer.Address2, customer.Address3)
    customer.BTStreet = joinAddress(customer.BTAddress1, customer.BTAddress2, customer.BTAddress3)
    for(let k = 1; k <= 3; k++) {
      delete customer['Address' + k]
      delete customer['BTAddress' + k]
    }
    customer.HasBTAddress = !!(customer.BTStreet || customer.BTCity || customer.BTState)
    return customer
  },
  prepare: function(customer, action) {
    // split address field into the address1, 2 and 3
    // populate the termscode, currencycode, vCustType and TerritoryID fields, based on the object values
    if(!customer.CustID && action === 'insert')
      customer.CustID = '0'
    if(customer.Street !== undefined)
      ;({0: customer.Address1, 1: customer.Address2, 2: customer.Address3} = splitAddress(customer.Street))
    if(customer.HasBTAddress !== undefined) {
      if(customer.HasBTAddress) {
        if(customer.BTStreet !== undefined)
          ;({0: customer.BTAddress1, 1: customer.BTAddress2, 2: customer.BTAddress3} = splitAddress(customer.BTStreet))
        if(!customer.BTName)
          customer.BTName = customer.Name
      } else {
        customer.BTAddress1 = null
        customer.BTAddress2 = null
        customer.BTAddress3 = null
        customer.BTCity = null
        customer.BTState = null
        customer.BTZip = null
        customer.BTName = null
      }
    }
    return customer
  },
  map: {
    CustNum: 'Id',
    CustURL: 'Website',
    Phone: 'PhoneNum',
    vCustType: 'Status'
  },
  joints: [
    // {
    //   lookupField: 'TermsCode', parentEntity: 'Terms', parentFieldName: 'Terms', parentFields: ['TermsCode', 'Description']
    // },
    // {
    //   lookupField: 'CurrencyCode', parentEntity: 'Currencies', parentFieldName: 'Currency', parentFields: ['CurrencyCode', 'CurrDesc']
    // },
    // {
    //   lookupField: 'TerritoryID', parentEntity: 'Territories', parentFieldName: 'Territory', parentFields: ['TerritoryID', 'TerritoryDesc']
    // },
    // {
    //   lookupField: 'ShipViaCode', parentEntity: 'ShipVia', parentFieldName: 'ShipVia', parentFields: ['ShipViaCode', 'Description']
    // }
  ]
}
