const customers = require('./customers'),
  suppliers = require('./suppliers'),
  resources = require('./resources'),
  roles = require('./roles'),
  lookupTables = require('./lookupTables'),
  addIdMapping = require('./utils/addIdMapping')
module.exports = [
  ...lookupTables,
  customers,
  suppliers,
  resources,
  roles
]
