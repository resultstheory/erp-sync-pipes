const joinAddress = require('./joinAddress.js'),
  splitAddress = require('./splitAddress.js'),
  mapCustType = require('./mapCustType.js')

module.exports = {
  local: 'Suppliers',
  remote: 'Supplier',
  fields: [
    'VendorNum', 'Company', 'VendorID', 'Name', 'PhoneNum', 'CustURL',
    'Address1', 'Address2', 'Address3', 'City', 'State', 'Zip',
    'TerritoryID', 'TermsCode', 'CurrencyCode', 'SysRevID'
  ],
  cleanse: function(supplier) {
    // clean address: combine the Address1, 2 and 3 and remove the original fields (?)
    supplier.Street = joinAddress(supplier.Address1, supplier.Address2, supplier.Address3)
    for(let k = 1; k <= 3; k++) {
      delete supplier['Address' + k]
    }
    return supplier
  },
  prepare: function(supplier, action) {
    // split address field into the address1, 2 and 3
    // populate the termscode, currencycode, vCustType and TerritoryID fields, based on the object values
    if(!supplier.VendorID && action === 'insert')
      supplier.VendorID = '0'
    if(supplier.Street !== undefined)
      ;({0: supplier.Address1, 1: supplier.Address2, 2: supplier.Address3} = splitAddress(supplier.Street))
    return supplier
  },
  map: {
    VendURL: 'Website',
    Phone: 'PhoneNum'
  },
  joints: [
    // {
    //   lookupField: 'TermsCode', parentEntity: 'Terms', parentFieldName: 'Terms', parentFields: ['TermsCode', 'Description']
    // },
    // {
    //   lookupField: 'CurrencyCode', parentEntity: 'Currencies', parentFieldName: 'Currency', parentFields: ['CurrencyCode', 'CurrDesc']
    // },
    // {
    //   lookupField: 'TerritoryID', parentEntity: 'Territories', parentFieldName: 'Territory', parentFields: ['TerritoryID', 'TerritoryDesc']
    // },
  ]
}
