module.exports = {
  local: 'Roles',
  remote: 'ResourceGroup',
  fields: [
    'ResourceGrpID',
    'Company',
    'Description',
    'JCDept',
    'ProdLabRate',
    'QProdLabRate',
    'RoleDayPayRate_c',
    'RoleDaySellRate_c',
    'RoleWeekPayRate_c',
    'RoleWeekSellRate_c',
    'RoleMonthPayRate_c',
    'RoleMonthSellRate_c',
    'RoleAnnualPayRate_c',
    'RoleAnnualSellRate_c',
    'Inactive',
    'CreateDate_c',
    'CreatedBy_c',
    'ModifyDate_c',
    'ModifiedBy_c',
    'InputWhse',
    'InputBinNum',
    'SplitBurden',
    'BurdenEQLabor'
  ],
  map: {
    Description: 'Name'
  }
}
